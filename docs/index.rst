Welcome to Debusine's documentation!
====================================

debusine is a general purpose software factory tailored to the needs
of a Debian-based distribution. It brings together:

* an artifact storage system
* a task scheduler
* a workflow management system

Documentation for Developers
----------------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   devel/why
   devel/goals
   devel/design
   devel/ontology
   devel/contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

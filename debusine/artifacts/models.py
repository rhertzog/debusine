# Copyright 2019 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Data models for the artifacts application."""

import os.path
import uuid

from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models


def random_uuid():
    """Generate a random UUID and return it as a string."""
    return str(uuid.uuid4())


class Artifact(models.Model):
    """
    Database model of an artifact.

    An artifact is a set of files constituting the output of some process.
    Different artifacts have been output by the same process should share a
    common ``type``.

    An artifact can be associated with arbitrary key-value data used to convey
    some meta-information about the files or about the process that generated
    those files.
    """

    id = models.BigAutoField(primary_key=True)  # noqa
    kind = models.CharField(max_length=200,
                            verbose_name='Type of the artifact')
    name = models.SlugField(max_length=400, default=random_uuid)
    data = JSONField(default=dict, blank=True)


class File(models.Model):
    """
    Database model of an artifact file.

    A file is always associated to a single artifact.
    """

    id = models.BigAutoField(primary_key=True)  # noqa
    artifact = models.ForeignKey(Artifact, on_delete=models.CASCADE)
    name = models.CharField(max_length=400)
    url = models.URLField(max_length=500, blank=True)
    path = models.CharField(max_length=500, blank=True)
    size = models.BigIntegerField(
        null=True, blank=True,
        validators=[MinValueValidator(0)],
    )

    def clean(self):
        """
        Enforce that we reference a file somewhere.

        Either remotely through the ``url`` field or locally with the ``path``
        field.
        """
        if self.url == "" and self.path == "":
            raise ValidationError(
                'At least one of `url` and `path` must be set.')
        super(File, self).clean()

    def save(self):
        """Initialize the ``size`` field when possible."""
        if self.size is None and self.path and os.path.exists(self.path):
            self.size = os.path.getsize(self.path)
        super(File, self).save()

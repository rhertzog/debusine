# Tox (http://tox.testrun.org/) is a tool for running tests
# in multiple virtualenvs. This configuration file will run the
# test suite on all supported python versions. To use it, "pip install tox"
# and then run "tox" from this directory.

# NOTE: python-apt must be available system-wide as it can't be installed
# by pip, alternatively you must list it as an external dependency
# with an URL like this one:
# http://snapshot.debian.org/archive/debian/20130606T215913Z/pool/main/p/python-apt/python-apt_0.8.9.1.tar.gz

[tox]
envlist =
    {py37}-{django22}-unit-tests,
    flake8,
# Ensure we have no warnings on last Django LTS
    py37-django22-no-warnings,
skipsdist = True
skip_missing_interpreters = True

[testenv]
whitelist_externals =
    tox
    xvfb-run
# The "-W ignore:::distutils:" parameter for the no-warnings call is due to this bug:
# https://github.com/pypa/virtualenv/issues/955
# The "-W ignore:::site:" parameter for the no-warnings call is due to this bug:
# https://github.com/pypa/virtualenv/issues/1120
# The "-W ignore:::jinja2.utils:" (+ same for runtime) parameter for the no-warnings
# call is needed until we have a version > 2.10.1 with the appropriate fix.
setenv = LANG=C
commands =
    unit-tests: {envpython} ./manage.py test debusine
    no-warnings: {envpython} -W error -W ignore:::distutils: -W ignore:::site: -W ignore:::jinja2.utils: -W ignore:::jinja2.runtime: ./manage.py test debusine
    show-warnings: {envpython} -W all ./manage.py test debusine
    functional-tests: xvfb-run {envpython} ./manage.py test functional_tests
    check: {envpython} ./manage.py check
sitepackages = True
deps =
    django22: Django>=2.2,<2.3
    functional: Django>=2.2,<2.3
    coverage: coverage
    tests: requests
    tests: django_jsonfield
    tests: django_debug_toolbar
    tests: PyYAML
    tests: python_debian
    functional-tests: selenium


[testenv:flake8]
commands = {envpython} -m flake8 debusine/
deps =
    flake8
    flake8-import-order
    flake8-builtins
    flake8-logging-format
    flake8-rst-docstrings
    flake8-docstrings

[flake8]
max-complexity = 12
max-line-length = 80
exclude = .git,.ropeproject,.tox,__pycache__,debusine/project/settings/local.py,docs/conf.py,*/migrations/*.py
ignore = 
# function name should be lowercase
    N802,
# variable in function should be lowercase
    N806,
# camelcase imported as lowercase
    N813,
# line break after binary operator
    W504,
# sphinx's roles unknown to flake8-rst-docstrings until
# https://github.com/peterjc/flake8-rst-docstrings/issues/7
    RST304,
# rules for flake8-docstrings
# First line should end with a period
    D400,
enable-extensions = G
application-import-names = debusine

[testenv:coverage]
basepython = python3.7
commands = 
    {envpython} -m coverage erase
    {envpython} -m coverage run -p ./manage.py test debusine
    {envpython} -m coverage combine
    {envpython} -m coverage html
